# Eslint Config

Default eslint-config for AB Dev

Add .npmrc file with the contents
```
@abdev:registry=https://gitlab.com/api/v4/groups/29647212/packages/npm/
```


Install it with
```
npm i @abdev/eslint-config -D
```

Include it using 
```
var ts = require('@abdev/eslint-config');
var html = require('@abdev/eslint-config/html');
var js = require('@abdev/eslint-config/js');
module.exports = {
  root:true,
  ignorePatterns: ['*.spec.ts'],
  overrides: [
    {
      files: [
        '*.js'
      ],
      ...js
    },
    {
      files: [
        '*.ts',
      ],
      ...ts
    },
    {
      files: [
        '*.html'
      ],
      ...html
    }
  ]
};
```
