module.exports =  {
      "parser": "@typescript-eslint/parser",
      "plugins": [
        "@typescript-eslint",
        "@angular-eslint/eslint-plugin",
      ],
      "parserOptions": {
        "project": [
          "tsconfig.json",
          "e2e/tsconfig.json"
        ],
        "createDefaultProgram": true
      },
      "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:@angular-eslint/ng-cli-compat",
        "plugin:@angular-eslint/ng-cli-compat--formatting-add-on",
        "plugin:@angular-eslint/template/process-inline-templates"
      ],
      "rules": {
        "@typescript-eslint/member-ordering": [
          "warn"
        ],
        "max-len": [
          "warn",
          {
            "code": 140
          }
        ],
        "object-curly-spacing": [
          "warn",
          "always",
          {
            "arraysInObjects": false,
            "objectsInObjects": false
          }
        ],
        "array-bracket-spacing": [
          "warn",
          "always",
          {
            "singleValue": false,
            "objectsInArrays": false,
            "arraysInArrays": false
          }
        ],
        "arrow-parens": [
          "warn",
          "always"
        ],
        "comma-dangle": [
          "warn",
          "always-multiline"
        ],
        "@typescript-eslint/unbound-method": [
          "error",
          {
            "ignoreStatic": true
          }
        ],
        "lines-between-class-members": [
          "warn",
          "always",
          {
            "exceptAfterSingleLine": true
          }
        ],
        "space-unary-ops": "warn",
        "@typescript-eslint/no-floating-promises": [
          "warn"
        ],
        "@typescript-eslint/type-annotation-spacing": [
          "warn",
          {
            "before": false,
            "after": true
          }
        ],
        "space-infix-ops": "warn",
        "@typescript-eslint/space-infix-ops": [
          "error"
        ],
        "function-call-argument-newline": [
          "warn",
          "consistent"
        ],
        "padded-blocks": [
          "warn",
          {
            "blocks": "never",
            "classes": "never"
          }
        ],
        "no-trailing-spaces": "warn",
        "no-multi-spaces": "warn",
        "no-mixed-spaces-and-tabs": "error",
        "@typescript-eslint/indent": [
          "warn",
          2
        ],
        "@typescript-eslint/quotes": [
          "warn",
          "single",
          {
            "allowTemplateLiterals": true,
            "avoidEscape": true
          }
        ],
        "quotes": "off",
        "no-underscore-dangle": "off",
        "prefer-const": "warn",
        "no-inner-declarations": "off",
        "@angular-eslint/component-class-suffix": [
          "error",
          {
            "suffixes": [
              "Page",
              "Component"
            ]
          }
        ],
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "indent": "off",
        "jsdoc/check-indentation": "error",
        "no-unused-expressions": "error",
        "no-use-before-define": "error",
        "semi": "error",
        "spaced-comment": [
          "warn",
          "always",
          {
            "markers": [
              "/"
            ]
          }
        ],
        '@angular-eslint/component-selector': 'off',
        '@typescript-eslint/no-unsafe-member-access': 'off',
        '@typescript-eslint/no-unsafe-call':'off',
        '@typescript-eslint/no-unsafe-assignment':'off',
        'eqeqeq': ['error', 'always'],
        'comma-spacing': 'off',
        '@typescript-eslint/comma-spacing':[ 'warn', {'before':false,'after':true}],
        'arrow-spacing': ['warn', {'before':true,'after':true}]
      }
    };